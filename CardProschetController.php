<?php

namespace app\controllers;

class CardProschetController extends \yii\web\Controller {

    public function actionIndex() {
        return $this->render('index');
    }

    /**
     * Добавление новой записи через Ajax
     * @return array
     */
    public function actionAdd() {

        $session = \Yii::$app->session;
        $idProject = (int) $session['project'];
        if (\Yii::$app->request->isAjax) {
            // $id - модель карточки

            $id = \Yii::$app->request->post('id');
            $date = \Yii::$app->request->post('date');
            $description = \Yii::$app->request->post('comment');
            
            $status_noviy = \app\models\ProjectProschet::find()->where(['project_id' => $idProject, 'name' => 'Новый'])->one()->id;

            $model = new \app\models\ProschetMain();
            $model->id_card = $id;
            $model->id_proschet_status = $status_noviy; // id статуса просчёта "Новый"
            $model->id_user = \Yii::$app->getUser()->identity->id;
            $model->description = '<span class="default-str">описание</span>';
            $model->stavka = '<span class="default-str">ставка</span>';
            $model->comment = '<span class="default-str">результат</span>';            

            $model->date_create = date('U');
            $model->date_modify = '';

            $model->save();
            
            //запись в историю статусов
            $model_history_proschet = new \app\models\HistoryProschet();
            $model_history_proschet->id_card = $model->id_card;
            $model_history_proschet->id_proschet = $model->id;
            $model_history_proschet->id_proschet_status = $model->id_proschet_status;
            $model_history_proschet->date_create = date('U');
            $model_history_proschet->save(); 

            // Возврат данных текущая дата, очистка комментов, колво дней
            $jsonData = json_encode([
                'date' => date('d.m.y'),
                'comment' => '',
            ]);

            return $jsonData;
        }
    }

    public function actionDelete() {
        if (\Yii::$app->request->isAjax) {
            $id = \Yii::$app->request->post('id');
            $model_id = \Yii::$app->request->post('model_id');
            $element = \app\models\ProschetMain::findOne($id);
          
            $element->delete();

            // Возврат данных текущая дата, очистка комментов, колво дней

            return $this->redirect(['/card/update', 'id' => \Yii::$app->request->get('id_card')]);
        }
    }
    
     public function actionSetArchive() {
            if (\Yii::$app->request->isAjax) {
                $id = \Yii::$app->request->post('id');
                $model_id = \Yii::$app->request->post('model_id');
                $id_user = \Yii::$app->getUser()->identity->id;
                $element = \app\models\ProschetMain::findOne($id);
                $date = date('U');           

                $element->archive = date('U');
                
                $element->save();
            }
        }
        
        public function actionUpdate() {

        if (\Yii::$app->request->isAjax) {
            $id = \Yii::$app->request->post('id');           
       
            $id_proschet  = \Yii::$app->request->get('id');             
            $id_card  = \Yii::$app->request->get('id_card');             
            $id_proschet_status = \Yii::$app->request->post('id_proschet_status');
            $description = \Yii::$app->request->post('description');
            $stavka = \Yii::$app->request->post('stavka');
            $comment = \Yii::$app->request->post('comment');
                
            
            

            // Выгрузка модели по ID
            $model = \app\models\ProschetMain::find()->where(['id' => $id_proschet])->one(); 
           
            if($id_proschet_status) {
                $model->id_proschet_status = $id_proschet_status;
                
                $line_duble = \app\models\HistoryProschet::find()->where(['id_card' => $id_card, 'id_proschet' => $id_proschet, 'id_proschet_status' => $id_proschet_status])->one();
                
                if(!$line_duble){
                $model_history_proschet = new \app\models\HistoryProschet();
                $model_history_proschet->id_card = $id_card;
                $model_history_proschet->id_proschet = $model->id;
                $model_history_proschet->id_proschet_status = $id_proschet_status;
                $model_history_proschet->date_create = date('U');
                $model_history_proschet->save(); 
                }

            }
            if($description) $model->description = $description;
            if($stavka) $model->stavka = $stavka;
            if($comment) $model->comment = $comment;
            $model->date_modify = date('U');            
          
            $model->save();

            // Возврат данных текущая дата
            $jsonData = json_encode([
                'date' => date('d.m.y'),
            ]);

            return $jsonData;
        }
    }

}

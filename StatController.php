<?php

namespace app\modules\statistica\controllers;

use Yii;
use yii\web\Controller;
use app\modules\statistica\models\FilterSessionModel;
use app\modules\statistica\models\CommonModel;
use yii\filters\AccessControl;
use app\modules\statistica\models\DataTime;
use app\models\UserItemProject;
use app\modules\statistica\models\SResultStatus;

class StatController extends Controller {

    public $filter = []; // – данные фильтра
    public $request;
    public $FilterModel;
    public $layout = '@app/views/layouts/main_lk.php';

    public function init() {
        parent::init();

        if (!\Yii::$app->getUser()->can('user')) {
            return $this->redirect(['/site/index']);
        }
        $this->FilterModel = new FilterSessionModel();
        $this->filter = $this->FilterModel->filter;
        $this->request = \Yii::$app->request;
    }

    public function actionIndex() {

        // если пользователь не зарегистрирован
        if (!Yii::$app->getUser()->can('user')) {
            return $this->redirect(['/site/login']);
        }

        // если у пользователя нет проекта
        if (!UserItemProject::findOne(['user_id' => Yii::$app->user->id])->prroject_id) {
            return $this->render('users_guest');
        }

        // устанавливаем проект при входе
        if ($this->request != NULL && $this->request->post('project')) {
            $this->FilterModel->setSession('project', $this->request->post('project'));
        }

        // фильтры статистики
        // если переход из users  по пользователю
        if ($this->request->post('from-users')) {
            $this->FilterModel->setSession('filter_users', $this->request->post('users'));
            header("Refresh:0");
        }
        if (\Yii::$app->request->isAjax) {
            $this->FilterModel->setSession('filter_status', $this->request->post('status'));
            $this->FilterModel->setSession('filter_users', $this->request->post('users'));

            if ($this->request->post('day')) {
                $this->FilterModel->setSession('filter_period', $this->request->post('day'));
                $this->FilterModel->setSession('filter_day', $this->request->post('day'));

                $post_time_start = $this->request->post('timestart');
                $post_time_end = $this->request->post('timeend');

                $dataTime = (new DataTime());
                $dataTime->setTimePeriodFilter($this->request->post('day'), $post_time_start, $post_time_end);

                return true;
            }
        }

        $calls_count = (new CommonModel())->getCallsCount($this->filter);
        $action_main_count = (new CommonModel())->actionMainCount30($this->filter);
        $card_opened = (new CommonModel())->getOpenedCard($this->filter);

        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $projectID = $this->filter['projectID'];
        $project = $this->getProjectName($projectID);
        $status = $this->filter['status'];
        $users = $this->filter['users'];

        $naznach_call = (new \app\modules\statistica\models\SNaznachen())->getBlockInfo($this->filter);
        $naznach_tasks = (new \app\modules\statistica\models\SNaznachen_tasks())->getBlockInfo($this->filter);
        $prosroch_call = (new \app\modules\statistica\models\SProsrochen())->getBlockInfo($this->filter);
        $prosroch_tasks = (new \app\modules\statistica\models\SProsrochen_tasks())->getBlockInfo($this->filter);
        $zaversh_call = (new \app\modules\statistica\models\SZavershen())->getBlockInfo($this->filter);
        $proschet = (new \app\modules\statistica\models\SProschet())->getBlockInfo($this->filter);
        $status_mail = (new \app\modules\statistica\models\SStatus())->getBlockInfo($this->filter);
        $statistic_call = (new \app\modules\statistica\models\SStatistic())->getBlockInfo($this->filter);
        $dobavlen = (new \app\modules\statistica\models\SDobavlen())->getBlockInfo($this->filter);
        $result = (new \app\modules\statistica\models\SResult())->getBlockInfo($this->filter);
        $tecusch_call = (new \app\modules\statistica\models\STecuschCall())->getBlockInfo($this->filter);
        $users_data = (new \app\modules\statistica\models\SUsers())->getBlockInfo($this->filter);

        // разделение полной и краткой статы
        if ($this->request->get('full')) {
            $result_get = $result;
            $statistic_call_get = $statistic_call;
            $status_mail_get = $status_mail;
            $calls_count_get = $calls_count;
            $card_opened_get = $card_opened;
        } else {
            $result_get = NULL;
            $statistic_call_get = NULL;
            $status_mail_get = NULL;
            $calls_count_get = NULL;
            $card_opened_get = NULL;
        }

        $data = [
            'filter' => $this->filter,
            'naznach_call' => $naznach_call,
            'naznach_tasks' => $naznach_tasks,
            'prosroch_call' => $prosroch_call,
            'prosroch_tasks' => $prosroch_tasks,
            'zaversh_call' => $zaversh_call,
            'proschet' => $proschet,
            'status_mail' => $status_mail_get,
            'statistic_call' => $statistic_call_get,
            'dobavlen' => $dobavlen,
            'result' => $result_get,
            'tecusch_call' => $tecusch_call,
            'users' => $users,
            'users_data' => $users_data,
            'calls_count' => $calls_count_get,
            'action_main_count' => $action_main_count,
            'card_opened' => $card_opened_get,
            'projectID' => $projectID,
            'project' => $project,
            'status' => $status,
        ];
        return $this->render('index', $data);
    }

    public function actionUsers() {

        if (!Yii::$app->getUser()->can('user')) {
            return $this->redirect(['/site/login']);
        }

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $session = \Yii::$app->session;

        // меняем отображение статусов
        if ($this->request->post('statuses')) {
            $value = '';
            $st = $this->request->post('statuses');

            foreach ($st as $item) {
                if (ArrayHelper::getValue($item, 'name') == 'status[]') {
                    $value .= ArrayHelper::getValue($item, 'value') . ",";
                };
            }

            if (($model = LkSettingsUsersStatus::findOne(['id_user' => \Yii::$app->user->id])) == null) {
                $model = new LkSettingsUsersStatus();
            }
            $model->users_status = trim($value, ",");
            $model->id_user = \Yii::$app->user->id;
            $model->date = time();
            $model->save();
        }

        $block = new \app\modules\statistica\models\SUsers();
        $users_data = $block->getFullInfo($this->filter);

        $project = $this->getProjectName($projectID);

        return $this->render('users', [
                    'users_data' => $users_data,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function actionTecusch() {

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        $block = new \app\modules\statistica\models\STecuschCall();
        $tecusch_call = $block->getFullInfo($this->filter);

        return $this->render('tecusch', [
                    'tecusch_call' => $tecusch_call,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function actionProsroch() {

        $block = new \app\modules\statistica\models\SProsrochen();
        $prosroch_call = $block->getFullInfo($this->filter);

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        $check_prosroch_call = isset($prosroch_call) ? $prosroch_call : null;
        return $this->render('prosroch', [
                    'prosroch_call' => $check_prosroch_call, // Блок - Просроченные звонки
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
                    'filter' => $this->filter,
        ]);
    }

    public function actionProsrochTasks() {

        $block = new \app\modules\statistica\models\SProsrochen_tasks();
        $prosroch_call = $block->getFullInfo($this->filter);

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        $check_prosroch_call = isset($prosroch_call) ? $prosroch_call : null;
        return $this->render('prosroch_tasks', [
                    'prosroch_tasks' => $check_prosroch_call, // Блок - Просроченные звонки
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
                    'filter' => $this->filter,
        ]);
    }

    public function actionNaznach() {

        $block = new \app\modules\statistica\models\SNaznachen();
        $call = $block->getFullInfo($this->filter);

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        return $this->render('naznach', [
                    'naznach_call' => $call,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function actionNaznachTasks() {

        $block = new \app\modules\statistica\models\SNaznachen_tasks();
        $call = $block->getFullInfo($this->filter);

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        return $this->render('naznach_tasks', [
                    'naznach_tasks' => $call,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function actionDobavlen() {

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        $block = new \app\modules\statistica\models\SDobavlen();
        $dobavlen = $block->getFullInfo($this->filter);

        return $this->render('dobavlen', [
                    'dobavlen' => $dobavlen,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function actionZaversh() {

        if (!Yii::$app->getUser()->can('user')) {
            return $this->redirect(['/site/login']);
        }

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        $block = new \app\modules\statistica\models\SZavershen();
        $zaversh_call = $block->getFullInfo($this->filter);

        return $this->render('zaversh', [
                    'zaversh_call' => $zaversh_call,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function actionZavershTasks() {

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        $block = new \app\modules\statistica\models\SZavershen_tasks();
        $zaversh_call = $block->getFullInfo($this->filter);

        return $this->render('zaversh_tasks', [
                    'zaversh_call' => $zaversh_call,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function actionSovershen() {

        $block = new \app\modules\statistica\models\SSovershen();
        $call = $block->getFullInfo($this->filter);
        return $call;
    }

    public function actionResult() {

        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

        $block = new \app\modules\statistica\models\SResult();
        $result = $block->getFullInfo($this->filter);

        return $this->render('result', [
                    'result' => $result,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function statusAction() {
        $this->filter = $this->filter->filter;
        $block = new \app\modules\statistica\models\SStatus();
        $call = $block->getFullInfo($this->filter);
        return $call;
    }

    public function actionProschet() {
        if (!Yii::$app->getUser()->can('user')) {
            return $this->redirect(['/']);
        }


        $projectID = $this->filter['projectID'];
        $timeStart = $this->filter['timeStart'];
        $timeEnd = $this->filter['timeEnd'];
        $users = $this->filter['users'];
        $status = $this->filter['status'];

        $project = $this->getProjectName($projectID);

//        $proschet = Proschet::getProschetCall($projectID, $project, $users, $status, $timeStart, $timeEnd );
        $block = new \app\modules\statistica\models\SProschet();
        $proschet = $block->getFullInfo($this->filter);

        return $this->render('proschet', [
                    'proschet' => $proschet,
                    'projectID' => $projectID,
                    'project' => $project, // Активный проект
                    'status' => $status, // Активный статус
                    'users' => $users, // Выбранный пользователь - массив
                    'calls_count' => (new CommonModel())->getCallsCount($this->filter), // общее количество звонков
                    'action_main_count' => (new CommonModel())->actionMainCount30($this->filter), // Количество важных действий
                    'card_opened' => (new CommonModel())->getOpenedCard($this->filter), // Количество карточек в которых работали
        ]);
    }

    public function getProjectName($projectID) {
        $name = isset(\app\models\Project::findOne($projectID)->name) ? \app\models\Project::findOne($projectID)->name : '';
        return $name;
    }

    public static function TimeRes($filter, $result_status) {
        $projectID = $filter['projectID'];
        $timeStart = $filter['timeStart'];
        $timeEnd = $filter['timeEnd'];
        $users = $filter['users'];
        $status = $filter['status'];

        $result = SResultStatus::getResult2($projectID, $result_status, $timeStart, $timeEnd);

        return count($result);
    }

}

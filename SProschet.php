<?php

namespace app\modules\statistica\models;

use Yii;
use app\models\Card;
use app\models\ProjectStatus;
use app\models\User;
use yii\db\Query;
use yii\helpers\Url;
use app\modules\statistica\models\DateModel;
use yii\helpers\ArrayHelper;

class SProschet extends CommonModel implements StatisticInterface {

    public function getBlockInfo($filter) {        
        
        $projectID = $filter['projectID'];
        $timeStart = $filter['timeStart'];
        $timeEnd = $filter['timeEnd'];
        $users = $filter['users'];
        $status = $filter['status'];

        $sum = 0;
        $result = array();

        $subQuery = $this->getCardProjectWithStatusQ($status, $projectID);

        $calls_1 = (new Query())
            ->select([ 'card_id', 'COUNT(card_id) AS cnt', 'MAX(date_create) AS date_create', 'MAX(user_id) AS user_id', 'MAX(comment) AS comment'])
            ->from('card_cheduled_calls')
            ->andWhere(['proschet'=>1])
            ->andWhere(['archive'=>null]) // исключение архивных                  
            ->andWhere(['user_id'=>$users])
            ->andWhere(['between', 'date_create', $timeStart, $timeEnd])
            ->innerJoin(['card' => $subQuery], 'card.id = card_id')
            ->groupBy('card_id')
            ->all();
        
        $calls_2 = (new Query())
                ->select(['id_card AS card_id', 'COUNT(id_card) AS cnt'])
                ->from('proschet_main')
                ->andWhere(['id_user' => $users])
                ->andWhere(['between', 'date_create', $timeStart, $timeEnd])
                ->innerJoin(['card' => $subQuery], 'card.id = id_card')
                ->addGroupBy('card_id')
                ->orderBy(['cnt' => SORT_DESC]) // вместо multisort     
                ->all();
        
        foreach ($calls_1 as $call) {
            if (Card::findOne($call['card_id'])->archive)  continue; // исключение архивных карт
            
            $result[$call['card_id']]['title'] = Card::findOne($call['card_id'])->title;
            $result[$call['card_id']]['url'] = Url::to(['/card/update', 'id' => $call['card_id']]);
            $result[$call['card_id']]['cnt'] = $call['cnt'];
            $sum += $call['cnt'];
                }
        
        foreach ($calls_2 as $call) {
            if (Card::findOne($call['card_id'])->archive)  continue; // исключение архивных карт
            
            $result[$call['card_id']]['title'] = Card::findOne($call['card_id'])->title;
            $result[$call['card_id']]['url'] = Url::to(['/card/update', 'id' => $call['card_id']]);
            $result[$call['card_id']]['cnt'] += $call['cnt'];
            $sum += $call['cnt'];
        }

        $result['sum'] = $sum;

        return $result;
    }

    public function getFullInfo($filter) {
        
        // если пользователь не зарегистрирован
        if (!Yii::$app->getUser()->can('user')) {
            exit('У вас нет прав доступа к данной странице');
        }
        
        $projectID = $filter['projectID'];
        $timeStart = $filter['timeStart'];
        $timeEnd = $filter['timeEnd'];
        $users = $filter['users'];
        $status = $filter['status'];

        $result = array();

        $subQuery = (new CommonModel())->getCardProjectWithStatusQ($status, $projectID);

        $calls_2 = (new Query())
                ->select(['id', 'id_card AS card_id', 'date_create', 'id_user AS user_id', 'description', 'stavka', 'comment'])
                ->from('proschet_main')  
                ->andWhere(['id_user' => $users])
                ->andWhere(['between', 'date_create', $timeStart, $timeEnd]);
        if ($status != null) {
            $calls_2 = $calls_2->andWhere(['id_proschet_status' => $status]);
        }
        $calls_2 = $calls_2->all();        

        $num = 1;
              
        foreach ($calls_2 as $call) {
            if (Card::findOne($call['card_id'])->archive)  continue; // исключение архивных карт
            // если карточка не текущего проекта
            if (Card::findOne($call['card_id'])->project_id != $projectID) {
                continue;
            }

            $status = \app\models\ProschetMain::findOne($call['id'])->id_proschet_status;
            ($status == null) ? $status = '' : $status = \app\models\ProjectProschet::findOne($status)->name;

            $result[$num]['id'] = $call['card_id'];
            $result[$num]['date_create'] = DataTime::getTimeFromUnix($call['date_create']);
            $result[$num]['date_create_sort'] = $call['date_create'];
            $result[$num]['title'] = Card::findOne($call['card_id'])->title;
            $result[$num]['description'] = $call['description'];
            $result[$num]['stavka'] = $call['stavka'];
            $result[$num]['comment'] = $call['comment'];
            $result[$num]['status'] = $status;
            $result[$num]['cnt'] = $num;
            $result[$num]['user'] = User::getUserById($call['user_id']);
            $result[$num]['url'] = Url::to(['/card/update', 'id' => $call['card_id']]);
            $num += 1;
        }

        Yii::debug($result, 'ddd $result');
        return $result;
    }

}

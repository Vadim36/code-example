<?php

namespace app\controllers;

use Zadarma_API\Api;
use Zadarma_API\ApiException;
use Zadarma_API\Client;
use app\models\Zadarma;
use app\models\CardContact;
use app\models\CardCalls;
use yii\filters\AccessControl;

define('KEY', '11111111111');
define('SECRET', '2222222222222');
define('SIP', '111');

class ZadarmaController extends \yii\web\Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'add-row', 'client-call', 'get-url-record', 'reload', 'rec'],
                        'roles' => ['user'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex() {
        $api = new Api(KEY, SECRET, true);

        $data_start = date('Y-m-d  H:i:s');
        $data_finish = date('Y-m-d H:i:s', strtotime("+30 min"));
        $search_phone = '380985714069';
        $search_sip = '105';
        try {
            $result = $api->getPbxStatistics("2019-07-22 13:47:07", "2019-07-22 13:47:55");

            $result1 = [];
            foreach ($result->stats as $value) {
                if ($value['destination'] == $search_phone) {
                    $result1[] = $value;
                }
            }
        } catch (ApiException $e) {
            echo 'Error: ' . $e->getMessage();
        }

        $data = [
            'result' => $result1,
        ];
        return $this->render('index', $data);
    }

    public function actionAddRow() {

        if (\Yii::$app->request->isAjax) {
            // $id_card - модель карточки
            // $number - номер телефона
            $id_card = \Yii::$app->request->post('id_card');
            $number = \Yii::$app->request->post('number');

            $id_contact = CardContact::find()->where(['phone' => $number, 'id_card' => $id_card])->one()->id;

            $model = new Zadarma();
            $model->state = '1';                 // состояние -
            $model->id_call_zadarma = '2';       //ID звонка в Zadarma -
            $model->id_card = $id_card;         //ID карточки
            $model->id_contact = $id_contact;            // ID контакта
            $model->id_user = \Yii::$app->getUser()->identity->id;
            $model->comment = '';               // комментарий -
            $model->number = $number;                // номер телефона
            $model->date = date('U');           // 'Дата нажатия кнопки звонок'
            $model->record = '6';                // Запись разговора -
            $model->save();

            return true;
        }
        return false;
    }

    public function actionClientCall() {
        if (\Yii::$app->request->isAjax) {
            $id_call = \Yii::$app->request->post('id_call');
            $search_phone = \Yii::$app->request->post('phone');

            $date_call = CardCalls::find()->where(['id' => $id_call])->one()->date_create;

            $data_start = date('Y-m-d H:i:s', $date_call - 70);
            $data_finish = date('Y-m-d H:i:s', $date_call + 70); // увеличиваем время на * сек

            $api = new Api(KEY, SECRET, false);

            $result = $api->getPbxStatistics($data_start, $data_finish);

            $result1 = [];
            foreach ($result->stats as $value) {
                if ($value['destination'] == $search_phone) {
                    $result1[] = $value;
                }
            }

            $stat_zadarma = array_pop($result1);
            $id_call_zadarma = $stat_zadarma['call_id'];
            $curent_call_answer = $stat_zadarma['disposition'];

            if ($id_call_zadarma) {
                $get_record = $api->getPbxRecord($id_call_zadarma, "", 86400);
            } else {
                return 'нет статистики звонка от zadarma: ' . $id_call_zadarma;
            }

            $model = Zadarma::find()->where(['id_card_calls' => $id_call])->one();

            $model->comment = $stat_zadarma['disposition'];
            $model->id_card_calls = $id_call;
            $model->id_call_zadarma = $id_call_zadarma;
            if ($get_record->link) {
                $model->record = $get_record->link;
            }

            if (!$model->save()) {
                echo 'no save';
            }

            return $curent_call_answer;
        }
    }

    public function actionGetUrlRecord() {
        if (\Yii::$app->request->isAjax) {
            try {
                $id_card_calls = $_POST['id_card_calls'];

                $model = Zadarma::find()->where(['id_card_calls' => $id_card_calls])->one();
                @$get_record = $model->record;
                $model_comment = $model->comment;
                if ($model_comment == 'busy') {
                    return 'Занято.';
                } elseif ($model_comment == 'no answer') {
                    return 'Нет ответа.';
                }
                if ($get_record) {
                    return $get_record;
                } else {
                    return $id_card_calls . ' ссылка записи пустая';
                }
            } catch (Exception $exc) {
                return $exc->getMessage();
            }
        }
    }

}
